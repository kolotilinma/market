//
//  SelfConfiguringCell.swift
//  Market
//
//  Created by Михаил Колотилин on 18.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

protocol SelfConfiguringCell {
    static var reuseId: String { get }
    func configure<U: Hashable>(with value: U)
}
