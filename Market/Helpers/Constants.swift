//
//  Constants.swift
//  Market
//
//  Created by Михаил Колотилин on 17.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import Foundation

public let kFILEREFERENCE = "gs://market-70f6a.appspot.com"
public let kALGOLIA_APP_ID = "10O4BJ2WA6"
public let kALGOLIA_ADMIN_KEY = "921899178b73f96021b20bb5476158e4" /// don't need in production
public let kALGOLIA_SEARCH_KEY = "944f6184d5a10ac0a47f7da206b3e481"

//Firebase Headers
public let kUSER_PATH = "User"
public let kCATEGORY_PATH = "Category"
public let kITEMS_PATH = "Items"
public let kBASKET_PATH = "Basket"

// Category
public let kNAME = "name"
public let kIMAGENAME = "imageName"
public let kOBJECTID = "objectId"

// Item
public let kCATEGORYID = "categoryId"
public let kDESCRIPTION = "description"
public let kPRICE = "price"
public let kIMAGELINKS = "imageLinks"

// Basket
public let kOWNERID = "ownerId"
public let kITEMIDS = "itemIds"

// User

public let kEMAIL = "email"
public let kFIRSTNAME = "firstName"
public let kLASTNAME = "lastName"
public let kFULLNAME = "fullName"
public let kCURRENTUSER = "currentUser"
public let kFULLADDRESS = "fullAddress"
public let kONBOARD = "onBoard"
public let kPURCHASEDITEMIDS = "purchasedItemIds"
