//
//  FirebaseCollectionReference.swift
//  Market
//
//  Created by Михаил Колотилин on 16.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import Foundation
import FirebaseFirestore

enum FCollectionReference: String {
    case User
    case Category
    case Items
    case Basket
}

func FirebaseReference(_ collectionReferance: FCollectionReference) -> CollectionReference {
    return Firestore.firestore().collection(collectionReferance.rawValue)
}
