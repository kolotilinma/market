//
//  Helpers.swift
//  Market
//
//  Created by Михаил Колотилин on 19.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import Foundation


func convertToCurrency(_ number: Double) -> String {
    let currencyFormatter = NumberFormatter()
    currencyFormatter.usesGroupingSeparator = true
    currencyFormatter.numberStyle = .currency
    currencyFormatter.locale = Locale.current
    
    let priceString = currencyFormatter.string(from: NSNumber(value: number))!
    
    return priceString
}
