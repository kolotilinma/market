//
//  ItemTableViewCell.swift
//  Market
//
//  Created by Михаил Колотилин on 18.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import SDWebImage

class ItemTableViewCell: UITableViewCell {

    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func generateCell(_ item: Item) {
        nameLabel.text = item.name
        descriptionLabel.text = item.description
        priceLabel.text = convertToCurrency(item.price) //"\(item.price!) $"
        priceLabel.adjustsFontSizeToFitWidth = true
        if item.imageLinks != nil && item.imageLinks.count > 0 {
            
            self.itemImageView.sd_setImage(with: URL(string: item.imageLinks.first!), completed: nil)
            self.itemImageView.contentMode = .scaleAspectFill
//            StorageService.shared.downloadImage(imageUrls: [item.imageLinks.first!]) { (result) in
//                switch result {
//                case .success(let images):
//                    self.itemImageView.image = images?.first
//                case .failure(_):
//                    print("Cant download image")
//                }
//            }
        }
        
//        itemImageView.image = item.image
    }

}
