//
//  ImageCollectionViewCell.swift
//  Market
//
//  Created by Михаил Колотилин on 20.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import SDWebImage

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    func setupImageWith(itemImage: String) {
        imageView.sd_setImage(with: URL(string: itemImage), completed: nil)
        imageView.contentMode = .scaleAspectFit
    }
    
}
