//
//  ItemViewController.swift
//  Market
//
//  Created by Михаил Колотилин on 20.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import JGProgressHUD

class ItemViewController: UIViewController {

    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var item: Item!
//    var itemImages: [UIImage] = []
    let hud = JGProgressHUD(style: .dark)
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    private let cellHeight: CGFloat = 196
    private let itemsPerRow: CGFloat = 1
    var rightBarButtonItemsIsActive = true
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
//        downloadPictures()
        navigationItem.leftBarButtonItems = [UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(backAction))]
        if rightBarButtonItemsIsActive {
            navigationItem.rightBarButtonItems = [UIBarButtonItem(image: UIImage(named: "addToBasket"), style: .plain, target: self, action: #selector(addToBasketPressed))]
        }
        
    }
    
    // MARK: - Setup UI
    fileprivate func setupUI() {
        if item != nil {
            self.title = item.name
            nameLabel.text = item.name
            priceLabel.text = convertToCurrency(item.price)
            descriptionTextView.text = item.description
        }
    }
    
    // MARK: - IBActions
    @objc func backAction()  {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addToBasketPressed()  {
        if MUser.currentUser() != nil {
            FirestoreService.shared.dowloadBasketFromFirestore(MUser.currentId()) { (result) in
                switch result {
                case .success(let basket):
                    basket.itemIds.append(self.item.id)
                    self.updateBasket(basket: basket, withValues: [kITEMIDS : basket.itemIds!])
                case .failure(let error):
                    print(error.localizedDescription)
                case .none:
                    self.createNewBasket()
                }
            }
        } else {
            showLoginView()
        }
    }
    
    // MARK: - Add to basket
    
    fileprivate func createNewBasket()  {
        let newBasket = Basket()
        newBasket.id = UUID().uuidString
        newBasket.ownerId = MUser.currentId()
        newBasket.itemIds = [self.item.id]
        FirestoreService.shared.saveBasketToFirestore(newBasket)
        self.hud.textLabel.text = "Added to basket!"
        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 2.0)
    }
    
    fileprivate func updateBasket(basket: Basket, withValues: [String: Any])  {
        FirestoreService.shared.updateBasketInFirestore(basket, withValues: withValues) { (result) in
            switch result {
            case .success(_):
                self.hud.textLabel.text = "Added to basket!"
                self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                self.hud.show(in: self.view)
                self.hud.dismiss(afterDelay: 2.0)
            case .failure(let error):
                self.hud.textLabel.text = "Error: \(error.localizedDescription)"
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.show(in: self.view)
                self.hud.dismiss(afterDelay: 2.0)
                print(error.localizedDescription)
            }
        }
    }
    
    // MARK: - DownloadPictures
//    fileprivate func downloadPictures() {
//        if item != nil && item.imageLinks != nil {
//            StorageService.shared.downloadImage(imageUrls: item.imageLinks) { (result) in
//                switch result {
//                case .success(let allImages):
//                    guard let images = allImages else { return}
//                    self.itemImages = images
//                    self.imageCollectionView.reloadData()
//                case .failure(let error):
//                    print(error.localizedDescription)
//                }
//            }
//        }
//    }
    
    //MARK: Show login view
    fileprivate func showLoginView() {
        let loginView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "loginView")
        self.present(loginView, animated: true, completion: nil)
    }
    
}

// MARK: - UICollectionViewDataSource
extension ItemViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return item.imageLinks.count == 0 ? 1 : item.imageLinks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ImageCollectionViewCell
        if item.imageLinks.count > 0 {
            cell.setupImageWith(itemImage: item.imageLinks[indexPath.row])
        }
        
        return cell
    }
    
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ItemViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let availableWidth = collectionView.frame.width - sectionInsets.left
        return CGSize(width: availableWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
