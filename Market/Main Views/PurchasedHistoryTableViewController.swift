//
//  PurchasedHistoryTableViewController.swift
//  Market
//
//  Created by Михаил Колотилин on 06.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import JGProgressHUD
import EmptyDataSet_Swift

class PurchasedHistoryTableViewController: UITableViewController {

    var itemArray: [Item] = []
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadItems()
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return itemArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ItemTableViewCell
        cell.generateCell(itemArray[indexPath.row])

        return cell
    }
    
    // MARK: - Load Items
    private func loadItems() {
        FirestoreService.shared.dowloadItems(MUser.currentUser()!.purchasedItemsIds) { (result) in
            switch result {
            case .success(let allItems):
                self.itemArray = allItems
                print("We have \(allItems.count) purchased items")
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: error.localizedDescription, Success: false)
            }
        }
    }

    // MARK: - Helpers
    
    private func showAlert(title: String, Success: Bool) {
        let hud = JGProgressHUD(style: .dark)
        if Success {
            hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        } else {
            hud.indicatorView = JGProgressHUDErrorIndicatorView()
        }
        hud.textLabel.text = title
        hud.show(in: self.view)
        hud.dismiss(afterDelay: 2.0)
    }
}

// MARK: - EmptyDataSet Delegate
extension PurchasedHistoryTableViewController: EmptyDataSetDelegate, EmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return NSAttributedString(string: "No items to display!")
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return #imageLiteral(resourceName: "emptyData")
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return NSAttributedString(string: "Please check back later")
    }
}
