//
//  BasketViewController.swift
//  Market
//
//  Created by Михаил Колотилин on 28.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import JGProgressHUD

class BasketViewController: UIViewController {

    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var basketTotalPriceLabel: UILabel!
    @IBOutlet weak var totalItemsLabel: UILabel!
    @IBOutlet weak var checkOutButtonOutlet: UIButton!
    
    var basket: Basket?
    var allItems: [Item] = []
    var purchasedItemIds: [String] = []
    
    var environment: String = PayPalEnvironmentNoNetwork {
        willSet (newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    var payPalConfig = PayPalConfiguration()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = footerView
        setupPayPal()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Check if user is logged in
        if MUser.currentUser() != nil {
            loadBasketFromFirestore()
        } else {
            self.updateTotalLabels(true)
        }
    }
    
    // MARK: - IBActions
    @IBAction func checkOutButtonPressed(_ sender: Any) {
        if MUser.currentUser()!.onBoard {
            
            payButtonPressed()
            
//            addItemsToPurchaseHistory(itemIds: self.purchasedItemIds)
//            emptyTheBasket()
        } else {
            self.showAlert(title: "Please complete you profile!", Success: false)
        }
    }
    
    // MARK: - Download basket
    fileprivate func loadBasketFromFirestore() {
        FirestoreService.shared.dowloadBasketFromFirestore(MUser.currentId()) { (result) in
            switch result {
            case .success(let basket):
                self.basket = basket
                self.getBasketItems()
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(title: error.localizedDescription, Success: false)
            case .none:
                print("Empty Busket")
            }
        }
    }
    
    fileprivate func getBasketItems() {
        if basket != nil {
            FirestoreService.shared.dowloadItems(basket!.itemIds) { (result) in
                switch result {
                case .success(let allItems):
                    self.allItems = allItems
                    self.updateTotalLabels(false)
                    self.tableView.reloadData()
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlert(title: error.localizedDescription, Success: false)
                }
            }
        }
    }
    
    // MARK: -  Helper functions
    fileprivate func updateTotalLabels(_ isEmpty: Bool) {
        if  isEmpty {
            totalItemsLabel.text = "0"
            basketTotalPriceLabel.text = returnBasketTotalPrice()
        } else {
            totalItemsLabel.text = "\(allItems.count)"
            basketTotalPriceLabel.text = returnBasketTotalPrice()
        }
        checkoutButtonStatusUpdate()
    }
    
    fileprivate func returnBasketTotalPrice() -> String {
        var totalPrice = 0.0
        for item in allItems {
            totalPrice += item.price
        }
        return "Total price: " + convertToCurrency(totalPrice)
    }
    
    fileprivate func showItemView(withItem: Item) {
        let itemVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "itemView") as! ItemViewController
        itemVC.item = withItem
        itemVC.rightBarButtonItemsIsActive = false
        self.navigationController?.pushViewController(itemVC, animated: true)
        
    }
    
    fileprivate func checkoutButtonStatusUpdate() {
        checkOutButtonOutlet.isEnabled = allItems.count > 0
        if checkOutButtonOutlet.isEnabled  {
            checkOutButtonOutlet.backgroundColor = #colorLiteral(red: 1, green: 0.4941176471, blue: 0.4745098039, alpha: 1)
        } else  {
            disableChecoutButton()
        }
    }
    
    fileprivate func disableChecoutButton() {
        checkOutButtonOutlet.isEnabled = false
        checkOutButtonOutlet.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
    }
    
    fileprivate func removeItemFromBasket(itemId: String) {
        for i in 0..<basket!.itemIds.count  {
            if itemId == basket?.itemIds[i] {
                basket?.itemIds.remove(at: i)
                return
            }
        }
    }
    
    fileprivate func tempFunction() {
        for item in allItems {
            purchasedItemIds.append(item.id)
        }
    }
    
    fileprivate func emptyTheBasket() {
        purchasedItemIds.removeAll()
        allItems.removeAll()
        tableView.reloadData()
        basket!.itemIds = []
        FirestoreService.shared.updateBasketInFirestore(basket!, withValues: [kITEMIDS : basket!.itemIds!]) { (result) in
            switch result  {
            case .success(_):
                self.getBasketItems()
            case .failure(let error):
                print(error.localizedDescription)
                self.showAlert(title: error.localizedDescription, Success: false)
            }
        }
    }
    
    fileprivate func addItemsToPurchaseHistory(itemIds: [String]) {
        if MUser.currentUser() != nil {
            let newItemIds = MUser.currentUser()!.purchasedItemsIds + itemIds
            updateCurrentUserInFirestore(withValues: [kPURCHASEDITEMIDS : newItemIds]) { (error) in
                if error != nil {
                    print("Error adding purchaseed items ", error!.localizedDescription)
                }
            }
        }
    }
    
    private func showAlert(title: String, Success: Bool) {
        let hud = JGProgressHUD(style: .dark)
        if Success {
            hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        } else {
            hud.indicatorView = JGProgressHUDErrorIndicatorView()
        }
        hud.textLabel.text = title
        hud.show(in: self.view)
        hud.dismiss(afterDelay: 2.0)
    }
    
    // MARK: - PayPal
    private func setupPayPal() {
        payPalConfig.acceptCreditCards  = false
        payPalConfig.merchantName = "iOS Mixa Market"
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        payPalConfig.payPalShippingAddressOption = .both
    }
    
    private func payButtonPressed() {
        var itemsToBuy: [PayPalItem] = []
        for item in allItems {
            let tempItem = PayPalItem(name: item.name, withQuantity: 1, withPrice: NSDecimalNumber(value: item.price), withCurrency: "USD", withSku: nil)
            purchasedItemIds.append(item.id)
            itemsToBuy.append(tempItem)
        }
        let subTotal = PayPalItem.totalPrice(forItems: itemsToBuy)
        // optional
        let shipingCost = NSDecimalNumber(string: "50.0")
        let tax = NSDecimalNumber(string: "5.00")
        
        let paymentDetails = PayPalPaymentDetails(subtotal: subTotal, withShipping: shipingCost, withTax: tax)
        let total = subTotal.adding(shipingCost).adding(tax)
        let payment =  PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Payment to Mixa Market", intent: .sale)
        payment.items = itemsToBuy
        payment.paymentDetails = paymentDetails
        if  payment.processable {
            let peymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(peymentViewController!, animated: true, completion: nil)
        } else {
            print("Payment not processable")
        }
    }
    
    
}

// MARK: - UITableViewDataSource
extension BasketViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ItemTableViewCell
        cell.generateCell(allItems[indexPath.row])
        return cell
    }
    
    
}

// MARK: - UITableViewDelegate
extension BasketViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let itemToDelete = allItems[indexPath.row]
            allItems.remove(at: indexPath.row)
            tableView.reloadData()
            removeItemFromBasket(itemId: itemToDelete.id)
            FirestoreService.shared.updateBasketInFirestore(basket!, withValues: [kITEMIDS : basket!.itemIds!]) { (result) in
                switch result {
                case .success(_):
                    self.getBasketItems()
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlert(title: error.localizedDescription, Success: false)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        showItemView(withItem: allItems[indexPath.row])
    }
    
}

//MARK: - PayPalPaymentDelegate
extension BasketViewController: PayPalPaymentDelegate {
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        paymentViewController.dismiss(animated: true) {
            // TODO: Send a confirmation email to the user about the sale
            self.addItemsToPurchaseHistory(itemIds: self.purchasedItemIds)
            self.emptyTheBasket()
        }
    }
    
    
}
