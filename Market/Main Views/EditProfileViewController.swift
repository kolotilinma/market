//
//  EditProfileViewController.swift
//  Market
//
//  Created by Михаил Колотилин on 06.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import JGProgressHUD

class EditProfileViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUserInfo()
    }
    
    
    // MARK: - IBActions
    @IBAction func saveButtonPressed(_ sender: Any) {
        dismissKeyboard()
        if textFieldsHaveText()  {
            let withValues = [kFIRSTNAME : nameTextField.text!, kLASTNAME : surnameTextField.text!, kONBOARD : true, kFULLADDRESS : addressTextField.text!, kFULLNAME : (nameTextField.text! + " " + surnameTextField.text!)] as [String : Any]
            updateCurrentUserInFirestore(withValues: withValues) { (error) in
                if error == nil {
                    self.showAlert(title: "Updated!", Success: true)
                } else {
                    print("Error updating user: ", error!.localizedDescription)
                    self.showAlert(title: error!.localizedDescription, Success: false)
                }
            }
        } else {
            self.showAlert(title: "All fields are required!", Success: false)
        }
    }
    
    @IBAction func logOutButtonPressed(_ sender: Any) {
        logOutUser()
    }
    
    // MARK: - Update UI
    private func loadUserInfo() {
        if MUser.currentUser() != nil {
            let currentUser = MUser.currentUser()
            nameTextField.text = currentUser?.firstName
            surnameTextField.text = currentUser?.lastName
            addressTextField.text = currentUser?.fullAdress
        }
    }
    
    // MARK: - Helpers
    private func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    private func textFieldsHaveText() -> Bool {
        return (nameTextField.text != "" && surnameTextField.text != "" && addressTextField.text != "")
    }
    
    private func logOutUser() {
        MUser.logOutCurrentUser { (error) in
            if error == nil {
                print("Logged out")
                self.navigationController?.popViewController(animated: true)
            } else {
                print("Error Login out: ", error!.localizedDescription)
                self.showAlert(title: error!.localizedDescription, Success: false)
            }
        }
    }
    
    private func showAlert(title: String, Success: Bool) {
        let hud = JGProgressHUD(style: .dark)
        if Success {
            hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        } else {
            hud.indicatorView = JGProgressHUDErrorIndicatorView()
        }
        hud.textLabel.text = title
        hud.show(in: self.view)
        hud.dismiss(afterDelay: 2.0)
    }
    
}
