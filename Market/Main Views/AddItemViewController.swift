//
//  AddItemViewController.swift
//  Market
//
//  Created by Михаил Колотилин on 18.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import Gallery
import JGProgressHUD
import NVActivityIndicatorView

class AddItemViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var category: Category!
    var gallery: GalleryController!
    let hud = JGProgressHUD(style: .dark)
    var activityIndicator: NVActivityIndicatorView?
    
    var itemImages: [UIImage?] = []
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let activityFrame = CGRect(x: self.view.frame.width / 2 - 30, y: self.view.frame.height / 2 - 30, width: 60, height: 60)
        activityIndicator = NVActivityIndicatorView(frame: activityFrame, type: .ballPulse, color: #colorLiteral(red: 1, green: 0.4941176471, blue: 0.4745098039, alpha: 1), padding: nil)
    }

    // MARK: - IBActions
    
    @IBAction func doneBarButtonPressed(_ sender: Any) {
        dismissKeyboard()
        if fieldsAreCompleted() {
            saveToFirebase()
            
        } else {
            print("Error all fiealds are required")
            self.hud.textLabel.text = "All fields are required!"
            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
            self.hud.show(in: self.view)
            self.hud.dismiss(afterDelay: 2.0)
        }
        
    }
    
    @IBAction func cameraButtonPressed(_ sender: Any) {
        itemImages = []
        showImageGallery()
    }
    
    @IBAction func backgroundTuped(_ sender: Any) {
        dismissKeyboard()
    }
    
    // MARK: - Helpers
    
    fileprivate func fieldsAreCompleted() -> Bool {
        return (titleTextField.text != "" && priceTextField.text != "" && descriptionTextView.text != "")
    }
    
    fileprivate func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    fileprivate func popTheView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Save Item
    fileprivate func saveToFirebase() {
        showLoadingIndicator()
        let item = Item()
        item.id = UUID().uuidString
        item.name = titleTextField.text!
        item.categoryId = category.id
        item.description = descriptionTextView.text!
        item.price = Double(priceTextField.text!)
        if itemImages.count > 0 {
            StorageService.shared.uploadImages(images: itemImages, itemID: item.id) { (result) in
                switch result {
                case .success(let imageLinkArray):
                    item.imageLinks = imageLinkArray
                    FirestoreService.shared.saveItemToFirestore(item)
                    AlgoliaService.shared.saveItemToAlgolia(item: item)
                    self.hideLoadingIndicator()
                    self.popTheView()
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                    self.hideLoadingIndicator()
                    //TODO: alert about intrnet connection
                }
            }
        } else {
            FirestoreService.shared.saveItemToFirestore(item)
            AlgoliaService.shared.saveItemToAlgolia(item: item)
            hideLoadingIndicator()
            popTheView()
        }
    }
    
    fileprivate func showLoadingIndicator() {
        if activityIndicator != nil {
            self.view.addSubview(activityIndicator!)
            activityIndicator?.startAnimating()
        }
    }
    
    fileprivate func hideLoadingIndicator() {
        if activityIndicator != nil {
            activityIndicator!.removeFromSuperview()
            activityIndicator!.stopAnimating()
        }
    }
    
    fileprivate func showImageGallery() {
        self.gallery = GalleryController()
        self.gallery.delegate = self
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.Camera.imageLimit = 6
        self.present(self.gallery, animated: true, completion: nil)
    }
    
}

// MARK: - GalleryControllerDelegate
extension AddItemViewController: GalleryControllerDelegate {
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        if images.count > 0 {
            Image.resolve(images: images) { (resolvedImages) in
                self.itemImages = resolvedImages
            }
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
