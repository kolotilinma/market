//
//  StorageService.swift
//  Market
//
//  Created by Михаил Колотилин on 18.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import Foundation
import FirebaseStorage

class StorageService {
    
    static let shared = StorageService()
    
    let storage = Storage.storage()
    
    func uploadImages(images: [UIImage?], itemID: String, completion: @escaping (Result<[String], Error>) -> Void) {
        if Reachabilty.HasConnection() {
            var uploadImagesCount = 0
            var imageLinkArray: [String] = []
            var nameSuffix = 0
            for image in images {
                let fileName = "ItemImages/" + itemID + "/" + "\(nameSuffix)" + ".jpg"
                let imageData = image?.jpegData(compressionQuality: 0.5)
                
                saveImageInFirebase(imageData: imageData!, fileName: fileName) { (result) in
                    switch result {
                    case .success(let imageLink):
                        imageLinkArray.append(imageLink)
                        uploadImagesCount += 1
                        if uploadImagesCount == images.count {
                            completion(.success(imageLinkArray))
                        }
                    case .failure(let error):
                        completion(.failure(error))
                        print("Error uploading image", error.localizedDescription)
                    }
                }
                nameSuffix += 1
            }
        } else {
            completion(.failure(FirebaseError.noInternetConnection))
        }
        
    }
    
    func saveImageInFirebase(imageData: Data, fileName: String, completion: @escaping (Result<String, Error>)  -> Void) {
        var task: StorageUploadTask!
        let storageRef = storage.reference(forURL: kFILEREFERENCE).child(fileName)
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        task = storageRef.putData(imageData, metadata: metadata, completion: { (metaData, error) in
            task.removeAllObservers()
            if error != nil {
                completion(.failure(error!))
                return
            }
            storageRef.downloadURL { (url, error) in
                guard let  downloadUrl = url else {
                    completion(.failure(error!))
                    return
                }
                completion(.success(downloadUrl.absoluteString))
            }
        })
    }
    
    func downloadImage(imageUrls: [String], completion: @escaping (Result<[UIImage]?, Error>) -> Void) {
        var imageArray: [UIImage] = []
        var downloadCounter = 0
        for link in imageUrls {
            let ref = Storage.storage().reference(forURL: link)
            let megaByte = Int64(3 * 1024 * 1024)
            let downloadQueue = DispatchQueue(label: "imageDownloadQueue")
            downloadQueue.async {
                downloadCounter += 1
                ref.getData(maxSize: megaByte) { (data, error) in
                    guard let imageData = data else{
                        completion(.failure(error!))
                        return
                    }
                    imageArray.append(UIImage(data: imageData)!)
                    if downloadCounter == imageArray.count {
                        DispatchQueue.main.async {
                            completion(.success(imageArray))
                        }
                    }
                }
            }
        }
    }
    
}
