//
//  FirestoreService.swift
//  Market
//
//  Created by Михаил Колотилин on 17.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import Foundation
import FirebaseFirestore

class FirestoreService {
    static let shared = FirestoreService()
    
    // MARK: - Categories func
    func dowloadCategoriesFromFirestore(completion: @escaping (_ categoryArray: (Result<[Category], Error>)) -> Void) {
        var categoryArray: [Category] = []
        FirebaseReference(.Category).getDocuments { (snapshot, error) in
            if let error = error {
                completion(.failure(error))
            }
            guard let snapshot = snapshot else {
                completion(.success(categoryArray))
                return
            }
            if !snapshot.isEmpty {
                for categoryDict in snapshot.documents {
                    categoryArray.append(Category(_dictionary: categoryDict.data() as NSDictionary))
                }
            }
            completion(.success(categoryArray))
        }
    }
    
    func saveCategoryToFirestore(_ category: Category) {
        let id = UUID().uuidString
        category.id = id
        FirebaseReference(.Category).document(id).setData(categoryDictionaryFrom(category) as! [String : Any])
    }
    
    private func categoryDictionaryFrom(_ category: Category) -> NSDictionary  {
        return NSDictionary(objects: [category.id, category.name, category.imageName!],
                            forKeys: [kOBJECTID as NSCopying, kNAME as NSCopying, kIMAGENAME as NSCopying])
    }
    
    // MARK: - Items func
    func dowloadItemsFromFirestore(_ withCategoryId: String, completion: @escaping (_ itemArray: (Result<[Item], Error>)) -> Void) {
        var itemArray: [Item] = []
        FirebaseReference(.Items).whereField(kCATEGORYID, isEqualTo: withCategoryId).getDocuments { (snapshot, error) in
            if let error = error {
                completion(.failure(error))
            }
            guard let snapshot = snapshot else {
                completion(.success(itemArray))
                return
            }
            if !snapshot.isEmpty {
                for itemDict in snapshot.documents {
                    itemArray.append(Item(_dictionary: itemDict.data() as NSDictionary))
                }
            }
            completion(.success(itemArray))
        }
    }
    
    func dowloadItems(_ withIds: [String], completion: @escaping (_ itemArray: (Result<[Item], Error>)) -> Void) {
        var count = 0
        var itemArray: [Item] = []
        if withIds.count > 0 {
            for itemId in withIds {
                FirebaseReference(.Items).document(itemId).getDocument { (snapshot, error) in
                    if let error = error {
                        completion(.failure(error))
                    }
                    guard let snapshot = snapshot else {
                        completion(.success(itemArray))
                        return
                    }
                    if snapshot.exists {
                        itemArray.append(Item(_dictionary: snapshot.data()! as NSDictionary))
                        count += 1
                    } else {
                        completion(.success(itemArray))
                    }
                    if count == withIds.count {
                        completion(.success(itemArray))
                    }
                }
            }
        } else {
            completion(.success(itemArray))
        }
    }
    
    func saveItemToFirestore(_ item: Item) {
//        let id = UUID().uuidString
//        item.id = id
        FirebaseReference(.Items).document(item.id).setData(itemDictionaryFrom(item) as! [String : Any])
    }
    
    func itemDictionaryFrom(_ item: Item) -> NSDictionary  {
        return NSDictionary(objects: [item.id!, item.categoryId!, item.name!, item.description!, item.price!, item.imageLinks!],
                            forKeys: [kOBJECTID as NSCopying,
                                      kCATEGORYID as NSCopying,
                                      kNAME as NSCopying,
                                      kDESCRIPTION as NSCopying,
                                      kPRICE as NSCopying,
                                      kIMAGELINKS as NSCopying])
    }
    
    // MARK: - Basket func
    func dowloadBasketFromFirestore(_ ownerId: String ,completion: @escaping (((Result<Basket, Error>))?) -> Void) {
        FirebaseReference(.Basket).whereField(kOWNERID, isEqualTo: ownerId).getDocuments { (snapshot, error) in
            if let error = error {
                completion(.failure(error))
            }
            guard let snapshot = snapshot else {
//                completion(nil)
                return
            }
            if !snapshot.isEmpty && snapshot.documents.count > 0 {
                let basket = Basket(_dictionary: snapshot.documents.first!.data() as NSDictionary)
                completion(.success(basket))
            } else {
                completion(nil)
            }
        }
    }
    
    func updateBasketInFirestore(_ basket: Basket, withValues: [String : Any],
                                 completion: @escaping (_ result: (Result<Bool, Error>)) -> Void) {
        FirebaseReference(.Basket).document(basket.id).updateData(withValues) { (error) in
            if let error = error {
                completion(.failure(error))
            }
            completion(.success(true))
        }
    }
    
    func saveBasketToFirestore(_ basket: Basket) {
        FirebaseReference(.Basket).document(basket.id).setData(basketDictionaryFrom(basket) as! [String : Any])
    }
    
    private func basketDictionaryFrom(_ basket: Basket) -> NSDictionary  {
        return NSDictionary(objects: [basket.id!, basket.ownerId!, basket.itemIds!],
                            forKeys: [kOBJECTID as NSCopying, kOWNERID as NSCopying, kITEMIDS as NSCopying])
    }
    
    
}

//func createCategorySet() {
//    let womenClothing = Category(_name: "Women's Clothing & Accessories", _imageName: "womenCloth")
//    let footWaer = Category(_name: "Footwaer", _imageName: "footWaer")
//    let electronics = Category(_name: "Electronics", _imageName: "electronics")
//    let menClothing = Category(_name: "Men's Clothing & Accessories" , _imageName: "menCloth")
//    let health = Category(_name: "Health & Beauty", _imageName: "health")
//    let baby = Category(_name: "Baby Stuff", _imageName: "baby")
//    let home = Category(_name: "Home & Kitchen", _imageName: "home")
//    let car = Category(_name: "Automobiles & Motorcyles", _imageName: "car")
//    let luggage = Category(_name: "Luggage & bags", _imageName: "luggage")
//    let jewelery = Category(_name: "Jewelery", _imageName: "jewelery")
//    let hobby =  Category(_name: "Hobby, Sport, Traveling", _imageName: "hobby")
//    let pet = Category(_name: "Pet products", _imageName: "pet")
//    let industry = Category(_name: "Industry & Business", _imageName: "industry")
//    let garden = Category(_name: "Garden supplies", _imageName: "garden")
//    let camera = Category(_name: "Cameras & Optics", _imageName: "camera")
//
//    let arrayOfCategories = [womenClothing, footWaer, electronics, menClothing, health, baby, home, car, luggage, jewelery, hobby, pet, industry, garden, camera]
//
//    for category in arrayOfCategories {
//        FirestoreService.shared.saveCategoryToFirestore(category)
//    }
//}
