//
//  AlgoliaService.swift
//  Market
//
//  Created by Михаил Колотилин on 06.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import Foundation
import InstantSearchClient

class AlgoliaService {
    
    static let shared = AlgoliaService()
    
    let client =  Client(appID: kALGOLIA_APP_ID, apiKey: kALGOLIA_ADMIN_KEY)
    let index = Client(appID: kALGOLIA_APP_ID, apiKey: kALGOLIA_ADMIN_KEY).index(withName: "item_name")
    
    private init() {}
    
    //MARK: - Functions
    func saveItemToAlgolia(item: Item)  {
        let index = AlgoliaService.shared.index
        let itemToSave = FirestoreService.shared.itemDictionaryFrom(item) as! [String : Any]
        index.addObject(itemToSave, withID: item.id, requestOptions: nil) { (content, error) in
            if error != nil {
                print("Error seving to angolia", error!.localizedDescription)
            } else {
                print("Added to angolia")
            }
        }
    }
    
    func searchAlgolia(searchString: String, completion: @escaping (_ itemArray: (Result<[String], Error>)) -> Void) {
        let index = AlgoliaService.shared.index
        var resultIds: [String] = []
        let query = Query(query: searchString)
        
        query.attributesToRetrieve = ["name", "description"]
        index.search(query) { (content, error) in
            if let error = error {
                completion(.failure(error))
            }
            
            guard let content = content else {
                completion(.success(resultIds))
                return
            }
            if !content.isEmpty {
                let cont = content["hits"] as! [[String : Any]]
                resultIds = []
                for result in cont {
                    resultIds.append(result["objectID"] as! String)
                }
                completion(.success(resultIds))
            } else {
                completion(.success(resultIds))
            }
        }
    }
    
}
