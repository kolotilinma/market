//
//  AppDelegate.swift
//  Market
//
//  Created by Михаил Колотилин on 15.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        initializePayPal()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


    // MARK: - PayPal Init
    func initializePayPal() {
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction : "AcHv6KO3Yl63KSJ7GhcMt8XNdGFjlrq_ecGYA4MMqsF_x57yvCDS5JjUaeqPp8RyjFLogJ1FrSAapc4l", PayPalEnvironmentSandbox : "sb-msyqf1682418@business.example.com"])
    }
}

